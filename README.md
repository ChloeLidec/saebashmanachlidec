# SaeBashLidec

Dépot contenant tous les fichiers nécessaires au sae de bash

## Comment reproduire l'installation
Nous partons d'un OS ubuntu 20.04.3 en 64 bit configuré sur une machine virtuelle avec l'installation normale réalisée.
Il faut pour reproduire cette installation:
- [ ] Télécharger le répertoire contenant les différents installeurs
- [ ] Extraire les fichiers dans un répertoire
- [ ] Rentrer dans ce répertoire et ouvrir un terminal à partir de ce dernier
- [ ] Effectuer la commande dans le terminal : $ chmod u+x droits.sh
- [ ] Effectuer ensuite la commande: $ ./droits.sh
Ce script lancera tout les sous-scripts en ayant donné auparavant les droits a ceux-ci ainsi qu'au fichier de tests.
Notez qu'il est également possible de lancer chaque script séparémment une fois que les droits leur ont été accordés.

## Tester cet environnement
Afin de tester si l'environnement permet d'effectuer les tâches demandées, un fichier test.sh est disponible. Les droits lui sont accordés lors de l'execution de droits.sh . Pour effectuer les tests:
- [ ] Effectuer la commande : $ ./test.sh qui lancera tous les sous-tests
- [ ] Attendre de voir les tests de java et docker se faire(bash étant déja fait car déja inclus dans la machine)
- [ ] code va se lancer et ouvrir un fichier qu'il faudra lancer avec F5

Note: si le test docker ne fonctionne pas, c'est que le groupe n'a pas été réinitialisé ainsi il suffit seuleument de relancer la session.

## Pourquoi ai-je fait ces choix

J'ai récupéré les lignes d'installations sur les cours déjà fait dans l'année ainsi que sur des documentations diverses pour certaines lignes.
J'ai choisi de créer un installeur automatique pour simplifier les démarches. Le fichier droits.sh permet donc de donner les droits d'execution et de lancer tous les sous-programmes d'installation. J'ai fait le choix de séparer en plusieurs programmes pour nous permettre une plus grande liberté de modification et d'adaptabilité et de récuperer chaque sous-programme si besoin. Il y a 5 parties pour représenter les 5 installations à faire. Nous installons 2 logiciels: Code et le docker respectivemment dans  code.sh et docker.sh. On a aussi dans le java.sh l'installation du compilateur.
- [ ] code.sh installe code, qui nous est nécessaire pour java et python. On a une update ici pour gérer en cas de lancement qui ne serait pas la première installation. J'ai choisi d'utiliser snap car c'est l'installation est plus simple à réaliser
- [ ] docker.sh nous permet d'installer le docker qui nous permettra ensuite d'utiliser Oracle pour la BDD
- [ ] java.sh installe le compilateur java ainsi que les extensions nécessaires à code pour utiliser java correctement.
- [ ] oracle.sh installe simplement un container oracle(sans le paramétrer)
- [ ] python.sh installe les extensions python nécessaires pour code ainsi que pylint et pytest qui permettent respectivemment de vérifier les bonnes pratiques sur nos programmes et de tester nos programmes.

Nous avons ensuite les fichiers de test avec en élément principal test.sh qui donne les droits et lance tous les programmes de tests. Executable.java et testpy.py sont deux fichiers inclus dans le dossier et qui sont créés pour permettre de tester les commandes demandées.
Les sous-scripts sont:
- [ ] testdock.sh qui lance docker run hello-world pour tester le docker
- [ ] testjava.sh qui compile le fichier  Executable.java et l'execute
- [ ] testpy.py qui est le programme python qui sera lancé par le fichier test.sh


- lien drive de la vidéo: https://drive.google.com/file/d/1IOsVQuo8CtzPkWHX-dK-yUVXEpvtPXAQ/view?usp=sharing
- lien git: https://gitlab.com/Dikawu/saebashmanachlidec
